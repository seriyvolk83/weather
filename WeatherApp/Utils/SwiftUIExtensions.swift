//
//  SwiftUIExtensions.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/2/22.
//

import UIKit
import SwiftUI

extension Color {

    /// Create color with sRGB value in 16-format, e.g. 0xFF0000 -> red color
    ///
    /// - Parameter hex: the color in hex
    public init(_ hex: Int) {
        let components = (
            r: Double((hex >> 16) & 0xff) / 255,
            g: Double((hex >> 08) & 0xff) / 255,
            b: Double((hex >> 00) & 0xff) / 255
        )
        self.init(red: components.r, green: components.g, blue: components.b)
        }

    /// The light blue color
    static var lightBlue: Color {
        return Color(0x00aaff)
    }
}

/// Show alert with given error message
///
/// - Parameters:
///   - errorMessage: the error message
///   - completion: the completion callback
public func showError(errorMessage: String, completion: (()->())? = nil) {
    if Thread.isMainThread {
        showAlert(NSLocalizedString("Error", comment: "Error alert title"), message: errorMessage, completion: completion)
    }
    else {
        DispatchQueue.main.async {
            showAlert(NSLocalizedString("Error", comment: "Error alert title"), message: errorMessage, completion: completion)
        }
    }
}

/// Shows an alert with the title and message.
///
/// - Parameters:
///   - title: the title
///   - message: the message
///   - buttonTitle: the button title
///   - completion: the completion callback
public func showAlert(_ title: String, message: String, buttonTitle: String = NSLocalizedString("OK", comment: "OK"), completion: (()->())? = nil) {
    UIViewController.getCurrentViewController()?.showAlert(title, message, buttonTitle: buttonTitle, completion: completion)
}

/// Extension to display alerts
extension UIViewController {

    /// Get currently opened view controller
    ///
    /// - Returns: the top visible view controller
    public class func getCurrentViewController() -> UIViewController? {
        let window = (UIApplication.shared.delegate?.window
                      ??
                      (UIApplication.shared.connectedScenes.first as? UIWindowScene)?.windows.first)
        // Get the root UIViewController and iterate through presented views
        if let rootController = window?.rootViewController {

            var currentController: UIViewController! = rootController

            // Each ViewController keeps track of the view it has presented, so we
            // can move from the head to the tail, which will always be the current view
            while( currentController.presentedViewController != nil ) {
                currentController = currentController.presentedViewController
            }
            return currentController
        }
        return nil
    }

    /// Displays alert with specified title & message
    ///
    /// - Parameters:
    ///   - title: the title
    ///   - message: the message
    ///   - buttonTitle: the button title
    ///   - completion: the completion callback
    public func showAlert(_ title: String, _ message: String, buttonTitle: String = NSLocalizedString("OK", comment: "OK"), completion: (()->())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default,
                                      handler: { (_) -> Void in
            alert.dismiss(animated: true, completion: nil)
            DispatchQueue.main.async {
                completion?()
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
