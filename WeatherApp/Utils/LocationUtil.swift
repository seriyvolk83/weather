//
//  LocationUtil.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/2/22.
//

import Foundation
import CoreLocation

/// Util that provides current device locations (single, or as a sequence)
open class LocationUtil: NSObject, CLLocationManagerDelegate {

    public static var shared = LocationUtil()
    public var manager: CLLocationManager?
    private var callback: ((CLLocation)->())?
    private var singleCallback: ((CLLocation)->())?

    /// Setup
    /// Can be used to set accuracy. If not called, then `...hundredMeters` accuracy will be used
    public func setup(withAccuracy locationAccuracy: CLLocationAccuracy = kCLLocationAccuracyHundredMeters, allowBackground: Bool = false) {
        guard manager == nil else { return }
        manager = CLLocationManager()
        manager?.delegate = self
        manager?.activityType = .fitness
        manager?.desiredAccuracy = locationAccuracy
        if #available(iOS 9.0, *) {
            manager?.allowsBackgroundLocationUpdates = allowBackground
        } else {
            // Fallback on earlier versions
        }
        if !(manager?.authorizationStatus == .authorizedAlways || manager?.authorizationStatus == .authorizedWhenInUse) {
            manager?.requestAlwaysAuthorization()
        }
    }

    /// Starts listening location
    /// Call .stop() to finish the listening
    public func start(_ callback: @escaping (CLLocation)->(), needToSetup: Bool = true) {
        if CLLocationManager.locationServicesEnabled() && needToSetup {
            setup()
        }
        self.callback = callback
        manager?.startUpdatingLocation()
    }

    /// Stops listening location
    public func stop() {
        self.callback = nil
        manager?.stopUpdatingLocation()
    }

    /// Get current location
    public func getCurrentLocation(_ callback: @escaping (CLLocation)->()) {
        if CLLocationManager.locationServicesEnabled() {
            setup()
        }
        singleCallback = callback
        manager?.startUpdatingLocation()
    }

    // MARK: - CLLocationManager delegate

    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            singleCallback?(location)
            singleCallback = nil
            if callback == nil {
                stop()
            }
            else {
                callback?(location)
            }
        }
    }

    // MARK: - Search locations

    /// Search locations by coordinates
    /// - Parameters:
    ///   - location: the location
    ///   - callback: the callback to return the data
    func search(coordinates location: CLLocation, callback: @escaping ([City]) -> Void) {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            if let error = error {
                showError(errorMessage: error.localizedDescription)
                callback([])
            }
            else {
                let list = placemarks?.map({City.from(placemark: $0)}) ?? []
                callback(list)
            }
        }
    }

    /// Search locations by name
    /// - Parameters:
    ///   - string: the string
    ///   - callback: the callback to return the data
    func search(byName string: String, callback: @escaping ([City]) -> Void) {
        getCoordinate(addressString: string) { values, error in
            if let error = error {
                print("ERROR: \(error)")
            }
            callback(values)
        }
    }

    /// Get locations by the name
    /// - Parameters:
    ///   - addressString: the address string
    ///   - completionHandler: the callback used to return the results
    private func getCoordinate(addressString: String, completionHandler: @escaping ([City], NSError?) -> Void ) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(addressString) { (placemarks, error) in
            if let error = error {
                completionHandler([], error as NSError?)
            }
            else {
                let list = placemarks?.map({City.from(placemark: $0)}) ?? []
                completionHandler(list, nil)
            }
        }
    }
}

extension City {

    /// Create `City` from `CLPlacemark`
    /// - Parameter placemark: the placemark to use
    static func from(placemark: CLPlacemark) -> City {
        let city = City.create(name: placemark.name ?? "-", location: placemark.location?.coordinate, temperatureValue: nil)
        return city
    }
}
