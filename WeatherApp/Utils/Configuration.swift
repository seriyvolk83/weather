//
//  Configuration.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/2/22.
//

import Foundation

/// App configuration from `configuration.plist`
class Configuration {

    /// `configuration.plist` content.
    var dict = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "configuration", ofType: "plist")!)

    /// singleton
    static let shared = Configuration()

    /// the API key
    static var apiKey: String {
        return shared.dict!["apiKey"] as? String ?? ""
    }

    /// the API base URL
    static var apiBase: String {
        return shared.dict!["apiBase"] as? String ?? ""
    }
}
