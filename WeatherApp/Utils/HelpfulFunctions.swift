//
//  HelpfulFunctions.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/2/22.
//

import Foundation

extension Int {

    /// Get uniform random value between 0 and maxValue
    ///
    /// - Parameter maxValue: the limit of the random values
    /// - Returns: random Int
    public static func rand(_ maxValue: Int = 1000000) -> Int {
        return Int.random(in: 0...maxValue)
    }

}

extension Float {

    /// Get uniform random value between 0 and maxValue
    ///
    /// - Parameter maxValue: the limit of the random values
    /// - Returns: random Float
    public static func rand(_ maxValue: UInt32 = 1) -> Float {
        let floating: UInt32 = 100
        return Float(Int.rand(Int(maxValue * floating))) / Float(floating)
    }
}

extension Double {

    /// Format temperature as a string, e.g. `31.0 -> "31"` or `23.5 -> "23.5"`
    /// - Returns: string
    public func toTemperatureString(digits: Int = 1) -> String {
        if self.isInteger() {
            return String.localizedStringWithFormat("%.f", rounded()) as String
        }
        else {
            let str = String.localizedStringWithFormat("%.\(digits)f", self) as String
            if str.hasSuffix(".0") {
                return String.localizedStringWithFormat("%.f", rounded()) as String
            }
            return str
        }
    }

    /// Check if the value is integer
    ///
    /// - Returns: true - if integer value, false - else
    public func isInteger() -> Bool {
        if self > Double(Int.max)
            || self < Double(Int.min) {
            print("ERROR: the value can not be converted to Int because it is greater/smaller than Int.max/min")
            return false
        }
        return  self == Double(Int(self))
    }
}

extension Date {

    static var isoFormatter: DateFormatter {
        let f = DateFormatter()
        f.dateStyle = .full
        f.timeStyle = .full
        return f
    }

    static var dateFormatter: DateFormatter {
        let f = DateFormatter()
        f.dateFormat = "dd.MM.yyyy HH:mm:ss"
        return f
    }

    static var ddmm: DateFormatter {
        let f = DateFormatter()
        f.dateFormat = "dd.MM"
        return f
    }

    static var hhmm: DateFormatter {
        let f = DateFormatter()
        f.dateFormat = "HH:mm"
        return f
    }

    static var ddmmhhmm: DateFormatter {
        let f = DateFormatter()
        f.dateFormat = "dd.MM HH:mm"
        return f
    }

    static func toString(_ date: Date?) -> String {
        if let date = date {
            return Date.dateFormatter.string(from: date)
        }
        return "-"
    }
}

extension String {

    /// Get string without spaces at the end and at the start.
    ///
    /// - Returns: trimmed string
    public func trim() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
}
