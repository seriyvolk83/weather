//
//  API.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/2/22.
//

import Foundation
import Combine

protocol API {

    /// Get current temperature in celsius
    /// - Returns: the temperature in celsius
    func getCurrentTemperature(for city: City) -> AnyPublisher<Temperature, Error>

    /// Get current temperature in celcius
    /// - Returns: the temperature in celcius
    func getTemperature(for city: City, start: Date, end: Date) -> [Temperature]
}

/// API implementation used for debugging and previews.
final class StubApi: API {

    /// Get current temperature in celsius
    /// - Returns: the temperature in celsius
    func getCurrentTemperature(for city: City) -> AnyPublisher<Temperature, Error> {
        return Future { promise in
            let t = Temperature.create(value: Double(Float.rand(26)), time: Date())
            promise(.success(t))
        }.eraseToAnyPublisher()
    }

    /// Get current temperature in celcius
    /// - Returns: the temperature in celcius
    func getTemperature(for city: City, start: Date, end: Date) -> [Temperature] {

        /// Generate `n` random values for every hour since now back to the past
        let n = 10
        var list = [Temperature]()
        let now = Date()
        for i in 0..<n {
            let date: Date = now.addingTimeInterval(-TimeInterval(i * 60 * 60))
            list.append(Temperature.create(value: Double(Float.rand(26)), time: date))
        }
        return list
    }
}
