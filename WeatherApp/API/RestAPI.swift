//
//  RestAPI.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/2/22.
//

import Foundation
import Combine
import SwiftEx83
import RxSwift

/// REST implementation of the API protocol
final class RestAPI: API {

    init() {}

    /// Get current temperature in celsius
    /// - Returns: the temperature in celsius
    func getCurrentTemperature(for city: City) -> AnyPublisher<Temperature, Error> {
        guard let location = city.location else {
            return Fail(error: "Location is not defined").eraseToAnyPublisher()
        }
        let url = url("/weather?lat=\(location.latitude)&lon=\(location.longitude)")
        return Future { promise in
            let r: Observable<WeatherResponse> = RestServiceApi.get(url: url, parameters: [:])
            _ = r.subscribe { value in
                let kelvin = value.main.temp
                promise(.success(Temperature.from(kelvin: kelvin)))
            } onError: { error in
                promise(.failure(error))
            }
        }.eraseToAnyPublisher()
    }

    /// Get current temperature in celcius
    /// - Returns: the temperature in celcius
    func getTemperature(for city: City, start: Date, end: Date) -> [Temperature] {
        return []
    }

    /// Get URL for the endpoint
    /// - Parameter endpoint: the endpoint
    private func url(_ endpoint: String) -> String {
        return "\(Configuration.apiBase)\(endpoint)&appid=\(Configuration.apiKey)"
    }

}
