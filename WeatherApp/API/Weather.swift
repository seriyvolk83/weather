//
//  Weather.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/2/22.
//

import Foundation

/// the API response model
struct WeatherResponse: Decodable {

    struct Main: Decodable {
        var temp: Double
    }

    struct Weather: Decodable {
        var id: Int
        var main: String
        var description: String?
        var icon: String?
    }

    var weather: [Weather]?
    var main: Main

    /// The other fields are not used for now, so skipping them. We need `main.temperature` only.
}


