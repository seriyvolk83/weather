//
//  WeatherDataSource.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/2/22.
//

import Foundation
import Combine
import CoreLocation

/// the main data source for the UI
final class WeatherDataSource: ObservableObject {

    internal let api: API
    private let cityService = CityStorage()
    private let temperatureService = TemperatureStorage()

    @Published var locations = [City]()

    @Published var loadingData: Bool = false
    @Published var loadingNewData: Bool = false

    /// Stores cancallable requests
    private var lastRequests = [UUID: AnyCancellable]()

    init(api: API, fetch: Bool = true) {
        self.api = api
        if fetch {
            fetchData()
        }
    }

    /// Fetch data from the storage
    private func fetchData() {
        fetchCities()
    }

    /// Fetch top statistics
    internal func fetchCities() {
        loadingData = true
        cityService.getAll({ [weak self] value in
            self?.loadingData = false
            self?.locations = value
        }, failure: { [weak self] error in
            showError(errorMessage: error.localizedDescription)
            self?.loadingData = false
        })
    }

    /// Unregister request
    /// - Parameter id: the request ID
    private func unregisterRequest(id: UUID) {
        self.lastRequests.removeValue(forKey: id)
    }

    /// Request weather data
    /// - Parameter item: the item
    func requestWeatherData(for item: SearchResult, callback: ((City) -> Void)?) {
        loadingNewData = true

        let requestId = UUID()
        let r = api.getCurrentTemperature(for: item).sink(receiveCompletion: { [weak self] completion in
            switch completion {
            case .finished:
                self?.unregisterRequest(id: requestId)
                break
            case .failure(let error):
                showError(errorMessage: error.localizedDescription)
                self?.loadingData = false
            }
        }, receiveValue: { [weak self] (value) in
            self?.upsertCity(city: item, temperature: value) { [weak self] updatedCity in
                let deferCallback = {
                    self?.loadingNewData = false
                    self?.unregisterRequest(id: requestId)
                }
                guard let updatedCity = updatedCity else { deferCallback(); return }
                self?.insertTemperature(for: updatedCity, temperature: value, callback: {
                    deferCallback()
                    callback?(updatedCity)
                })
            }
        })
        lastRequests[requestId] = r
    }

    /// Request data for the given location
    /// - Parameter location: the location
    func requestWeatherData(for location: CLLocation, callback: @escaping (City) -> Void) {
        loadingNewData = true
        LocationUtil.shared.search(coordinates: location) { [weak self] list in
            guard let city = list.first else {
                showError(errorMessage: "Cannot find a place for current coordinates")
                self?.loadingNewData = false
                return
            }
            self?.requestWeatherData(for: city, callback: callback)
        }
    }

    /// Add/update city
    private func upsertCity(city: City, temperature: Temperature, callback: @escaping (City?) -> Void) {
        var cityToUpdate: City!

        // Update city model properly
        if let cityExisting = locations.filter({$0.match(city: city)}).first {
            cityExisting.lastTemperatureValue = temperature.value
            cityExisting.updatedAt = Date()
            cityExisting.numberOfData += 1
            cityToUpdate = cityExisting
        }
        else {
            city.lastTemperatureValue = temperature.value
            city.numberOfData = 1
            cityToUpdate = city
        }

        /// Insert or update the city
        cityService.upsert([cityToUpdate], success: { [weak self] list in
            guard let updatedCity = list.first else { print("ERROR: should return updated object"); callback(nil); return }

            /// Update `locations`
            if let cityExisting = self?.locations.filter({$0.match(city: city)}).first, let index = self?.locations.firstIndex(of: cityExisting) {
                self?.locations[index] = updatedCity
            }
            else {
                self?.locations.append(updatedCity)
            }
            callback(updatedCity)
        }, failure: { error in
            showError(errorMessage: error.localizedDescription)
            callback(nil)
        })
    }

    /// Insert temperature data into the storage
    /// - Parameters:
    ///   - city: the related city
    ///   - temperature: the temperature to insert
    ///   - callback: the callback to call when ended
    private func insertTemperature(for city: City, temperature: Temperature, callback: @escaping () -> Void) {
        temperature.cityId = city.id
        /// Insert
        temperatureService.insert([temperature], success: { list in
            guard let _ = list.first else { print("ERROR: should return updated object"); callback(); return }
            callback()
        }, failure: { error in
            showError(errorMessage: error.localizedDescription)
            callback()
        })
    }
}

extension Publisher {

    /// Sink, check success completion and show error if needed
    /// - Parameters:
    ///   - receiveValue: The closure to execute on receipt of a value.
    /// - Returns: A cancellable instance, which you use when you end assignment of the received value. Deallocation of the result will tear down the subscription stream.
    public func sinkWithChecks(receiveValue: @escaping ((Self.Output) -> Void)) -> AnyCancellable {
        self.sink(receiveCompletion: { completion in
            switch completion {
            case .finished:
                break
            case .failure(let error):
                showError(errorMessage: error.localizedDescription)
            }
        }, receiveValue: receiveValue)
    }
}
