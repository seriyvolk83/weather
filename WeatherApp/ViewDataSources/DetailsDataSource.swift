//
//  DetailsDataSource.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/3/22.
//

import Foundation
import Combine

/// the data source for the details
final class DetailsDataSource: ObservableObject {

    @Published var city: City?
    @Published var data: [Temperature] = []

    @Published var loadingData: Bool = false

    @Published var minTemperature: Temperature?
    @Published var maxTemperature: Temperature?

    private let temperatureService = TemperatureStorage()

    init(city: City?) {
        self.city = city
        fetchData()
    }

    /// Fetch data from local storage
    private func fetchData() {
        loadingData = true
        temperatureService.get(withPredicate: temperatureService.createStringPredicate("cityId", value: city?.id ?? "")) { [weak self] list in
            self?.data = list.sorted(by: {$0.createdAt ?? Date() < $1.createdAt ?? Date()})
            var minTemp: Temperature?
            var maxTemp: Temperature?
            for t in list {
                if minTemp == nil || minTemp!.value > t.value {
                    minTemp = t
                }
                if maxTemp == nil || maxTemp!.value < t.value {
                    maxTemp = t
                }
            }
            self?.minTemperature = minTemp
            self?.maxTemperature = maxTemp
            self?.loadingData = false
        } failure: { [weak self] error in
            showError(errorMessage: error.localizedDescription)
            self?.loadingData = false
        }
    }
}
