//
//  SearchDataSource.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/2/22.
//

import Foundation
import Combine

typealias SearchResult = City

/// the search data source
final class SearchDataSource: ObservableObject {

    @Published var searchString: String = ""
    @Published var results = [SearchResult]()

    @Published var loadingData: Bool = false

    /// true - if search was complete and nothing is found, false - else
    var isNothingFound: Bool {
        return results.isEmpty && !searchString.trim().isEmpty
    }

    /// Search locations by string
    /// - Parameter text: the text to search
    func search(for text: String) {
        loadingData = true
        LocationUtil.shared.search(byName: text) { [weak self] list in
            self?.loadingData = false
            self?.searchString = text
            self?.results = list
        }
    }
}
