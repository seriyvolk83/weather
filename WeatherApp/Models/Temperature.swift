//
//  Temperature.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/2/22.
//

import Foundation

/// The temperature model
public class Temperature: CacheableObject {

    /// the temperature value in celsius
    var value: Double = Double.nan
    var cityId: String?

    /// the date of the temperature
    var time: Date {
        return createdAt ?? Date()
    }

    static func create(value: Double, time: Date) -> Temperature {
        let t = Temperature(id: UUID().uuidString)
        t.value = value
        t.createdAt = time
        return t
    }
}

extension Temperature {

    var celsius: Double {
        return value
    }

    var fahrenheit: Double {
        let base = UnitTemperature.celsius.converter.baseUnitValue(fromValue: value)
        return UnitTemperature.fahrenheit.converter.value(fromBaseUnitValue: base)
    }

    func toString(isCelsius: Bool, digits: Int = 1) -> String {
        return isCelsius ? "\(celsius.toTemperatureString(digits: digits))°C" : "\(fahrenheit.toTemperatureString(digits: digits))°F"
    }

    /// Create `Temperature` from kelvins
    /// - Parameter kelvin: the kelvins
    static func from(kelvin: Double) -> Temperature {
        let base = UnitTemperature.kelvin.converter.baseUnitValue(fromValue: kelvin)
        let c = UnitTemperature.celsius.converter.value(fromBaseUnitValue: base)
        let t = Temperature(id: UUID().uuidString)
        t.value = c
        return t
    }
}
