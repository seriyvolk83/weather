//
//  City.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/2/22.
//

import Foundation
import CoreLocation

/// The location (city) model - name and geo coordinate.
public class City: CacheableObject, Identifiable {

    /// the name of the city
    var name = ""
    /// the location of the city. `-1` is used because Core Data model has non-nil fields
    var locationLat: Double = Double.nan
    var locationLon: Double = Double.nan

    /// The last captured temperature. Duplicates last data from Temperature storage for quick rendering,
    var lastTemperatureValue: Double = Double.nan

    /// The number of data collected. Used to quickly show/hide chart button
    var numberOfData: Int = 0

    var updated: Date {
        updatedAt ?? createdAt ?? Date()
    }

    /// Check if it's the same city. This method is used instead of the standard `Hashable` vars/methods because after creating an object it may have different ID.
    /// We pay this price for the simplicity of city storage (we have to use `CacheableObject` which ID field and uses it in `Hashable`).
    /// - Parameter city: the city to check
    /// - Returns: true - if it's the same location, false - else
    func match(city: City) -> Bool {
        city.name == name
        && city.locationLat == locationLat
        && city.locationLon == locationLon
    }

    static func create(name: String = "", location: CLLocationCoordinate2D? = nil, temperatureValue: Double? = nil) -> City {
        let o = City(id: UUID().uuidString)
        o.name = name
        o.locationLat = location?.latitude ?? Double.nan
        o.locationLon = location?.longitude ?? Double.nan
        o.lastTemperatureValue = temperatureValue ?? Double.nan
        return o
    }
}

extension City {

    var location: CLLocationCoordinate2D? {
        guard !locationLat.isNaN, !locationLon.isNaN else { return nil }
        return CLLocationCoordinate2D(latitude: locationLat, longitude: locationLon)
    }

    var temperature: Temperature? {
        guard !lastTemperatureValue.isNaN else { return nil }
        return Temperature.create(value: lastTemperatureValue, time: updatedAt ?? createdAt ?? Date())
    }
}
