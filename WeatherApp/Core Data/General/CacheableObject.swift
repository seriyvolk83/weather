//
//  CacheableObject.swift
//
//  Created by Volkov Alexander on 10/16/18.
//  Modified by Volkov Alexander on 1/13/19.
//  Copyright © 2018-2019 Volkov Alexander. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreData

public typealias ObjectVersion = Date
/**
 * Abstract object for all cacheable model objects
 *
 * - author: Volkov Alexander
 * - version: 1.1
 */
/* changes:
 * 1.1:
 * - warning fixed
 */
public class CacheableObject: NSObject, CoreDataEntityBridge, Codable {

    // the ID
    public var id: String

    //////////////////////////////////////////////////////////////////
    // LOCAL FIELDS (see CoreDataEntityBridge)
    public var deletedLocally: Bool = false
    // The ObjectID of the CoreData object we saved to or loaded from
    open var managedObjectID: NSManagedObjectID?
    /// the date of data retrieval
    open var retrievalDate: Date = Date()

    /// the creation date
    public var createdAt: Date?
    /// the object update date
    public var updatedAt: Date?
    /// the current version of the project
    open var version: ObjectVersion?

    /// Initializer
    ///
    /// - Parameter id: ID
    public init(id: String) {
        self.id = id
    }

    /// hash value
    public override var hash: Int {
        return id.hashValue
    }

    /// Method can be overridden
    ///
    /// - Parameter json: JSON
    public func fillCommonFields(fromJson json: JSON) {
    }

    public enum CodingKeys: String, CodingKey {
        case id
    }


    // https://stackoverflow.com/questions/44553934/using-decodable-in-swift-4-with-inheritance
    required convenience public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.init(id: try container.decode(String.self, forKey: .id))
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
    }
}

/**
 Equatable protocol implementation

 - parameter lhs: the left object
 - parameter rhs: the right object

 - returns: true - if objects are equal, false - else
 */
public func ==<T: CacheableObject>(lhs: T, rhs: T) -> Bool {
    return lhs.hash == rhs.hash
}
