//
//  CacheableObjectMO.swift
//
//  Created by Volkov Alexander on 10/16/18.
//  Copyright © 2018 Volkov Alexander. All rights reserved.
//

import Foundation

/**
 * Methods for using by concrete subclasses of CacheableObjectMO
 *
 * - author: Volkov Alexander
 * - version: 1.0
 */
extension CacheableObjectMO {
    
    /// Update entity
    ///
    /// - Parameters:
    ///   - object: the object
    public func updateEntity(object: CoreDataEntityBridge) {
        var object = object
        object.deletedLocally = deletedLocally
        object.retrievalDate = retrievalDate ?? Date()

        object.managedObjectID = self.objectID
        
        object.createdAt = createdAt ?? Date()
        object.updatedAt = updatedAt ?? Date()
        if let version = version { object.version = version as ObjectVersion }
    }

    /// Fill fields of Core Data object from model object
    ///
    /// - Parameters:
    ///   - object: the object
    ///   - relatedObjects: the related objects (optional)
    public func fillDataFromCacheableObject(_ object: CoreDataEntityBridge, relatedObjects: Any?) {
        id = object.id
        createdAt = object.createdAt ?? Date()
        updatedAt = object.updatedAt ?? Date()

        deletedLocally = object.deletedLocally
        retrievalDate = object.retrievalDate
        version = object.version
    }
}

