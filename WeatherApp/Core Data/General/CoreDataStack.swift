//
//  CoreDataStack.swift
//
//  Created by Volkov Alexander on 10/16/18.
//  Copyright © 2018 Volkov Alexander. All rights reserved.
//

import Foundation
import CoreData

/**
 * Helpful extension for NSManagedObject
 *
 * - author: Volkov Alexander
 * - version: 1.0
 */
extension NSManagedObject {
    
    /// returns an instance of the object from context matching current thread
    ///
    /// - Parameter context: specific context
    /// - Returns: same object on specified context
    func `in`(context: NSManagedObjectContext = CoreDataStack.shared.viewContext) -> Self {
        return inContext(type: type(of: self), context: context)
    }
    
    // facilitates method above
    private func inContext<T>(type: T.Type, context: NSManagedObjectContext = CoreDataStack.shared.viewContext) -> T {
        return context.object(with: self.objectID) as! T
    }
}

/**
 * Utility that supports Core Data stack
 *
 * - author: Volkov Alexander
 * - version: 1.0
 */
public final class CoreDataStack {

    /// shared instanace
    static let shared: CoreDataStack = CoreDataStack(modelName: "Weather")

    /// Represents the model name property.
    public let modelName: String

    /**
     Initialize new instance with model name and concurrency type.

     - parameter modelName: the model name parameter.

     - returns: The new created instance.
     */
    public init(modelName: String) {
        self.modelName = modelName;
    }
    
    /// the error handler
    var errorHandler: (Error) -> Void = {error in fatalError("Unresolved error \(error)") }
    
    // MARK: - Core Data stack
    
    /// persistent container
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: modelName)
        container.loadPersistentStores(completionHandler: { [weak self] (storeDescription, error) in
            if let error = error as NSError? {
                NSLog("CoreData error \(error), \(String(describing: error._userInfo))")
                self?.errorHandler(error)
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
            }
        })
        return container
    }()
    
    /// view context
    lazy var viewContext: NSManagedObjectContext = {
        return self.persistentContainer.viewContext
    }()
    
    // Optional background context for background operations
    lazy var backgroundContext: NSManagedObjectContext = {
        return self.persistentContainer.newBackgroundContext()
    }()
    
    /// Perform foreground task
    ///
    /// - Parameter callback: the callback to invoke after the operation is done
    func performForegroundTask(_ callback: @escaping (NSManagedObjectContext) -> Void) {
        self.viewContext.perform {
            callback(self.viewContext)
        }
    }
    
    /// Perform background task
    ///
    /// - Parameter callback: the callback to invoke after the operation is done
    func performBackgroundTask(_ block: @escaping (NSManagedObjectContext) -> Void) {
        self.persistentContainer.performBackgroundTask(block)
    }
}

