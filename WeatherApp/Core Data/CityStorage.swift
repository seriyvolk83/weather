//
//  CityStorage.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/3/22.
//

import Foundation

/// Model object for Core Data related to City
extension CityMO: CoreDataEntity {

    /// Convert to enity
    ///
    /// - Returns: the entity
    public func toEntity() -> City {
        let object = City(id: self.id ?? "")
        updateEntity(object: object)

        object.name = name ?? ""
        object.locationLat = locationLat
        object.locationLon = locationLon
        object.lastTemperatureValue = lastTemperatureValue
        object.numberOfData = Int(numberOfData)
        return object
    }

    /// Update fields from given object
    ///
    /// - Parameters:
    ///   - object: the object
    ///   - relatedObjects: the related objects
    public func fillDataFrom(_ object: City, relatedObjects: Any?) {
        super.fillDataFromCacheableObject(object, relatedObjects: relatedObjects)

        name = object.name
        locationLat = object.locationLat
        locationLon = object.locationLon
        lastTemperatureValue = object.lastTemperatureValue
        numberOfData = Int64(object.numberOfData)
    }
}

/// Service caching Cities
class CityStorage: DataService<CityMO, City> {}
