//
//  TemperatureStorage.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/3/22.
//

import Foundation

/// Model object for Core Data related to Temperature
extension TemperatureMO: CoreDataEntity {

    /// Convert to enity
    ///
    /// - Returns: the entity
    public func toEntity() -> Temperature {
        let object = Temperature(id: self.id ?? "")
        updateEntity(object: object)

        object.value = value
        object.cityId = cityId
        return object
    }

    /// Update fields from given object
    ///
    /// - Parameters:
    ///   - object: the object
    ///   - relatedObjects: the related objects
    public func fillDataFrom(_ object: Temperature, relatedObjects: Any?) {
        super.fillDataFromCacheableObject(object, relatedObjects: relatedObjects)

        value = object.value
        cityId = object.cityId
    }
}

/// Service caching Cities
class TemperatureStorage: DataService<TemperatureMO, Temperature> {}
