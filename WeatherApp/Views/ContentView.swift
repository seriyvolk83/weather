//
//  ContentView.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/2/22.
//

import SwiftUI

/// Main view of the app
struct ContentView: View {

    var body: some View {
        HomeView()
            .environment(\.horizontalSizeClass, .compact)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
