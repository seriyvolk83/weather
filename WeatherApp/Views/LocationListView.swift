//
//  LocationListView.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/2/22.
//

import SwiftUI

/// A list of locations (below the main view)
struct LocationListView: View {

    @ObservedObject var data: WeatherDataSource
    @Binding var isCelsius: Bool
    @Binding var selected: City?
    @Binding var showDetails: City?

    var body: some View {
        List {
            ForEach(data.locations) { city in
                Button {
                    if selected == nil {
                        withAnimation {
                            selected = city
                        }
                    }
                    else {
                        selected = city
                    }
                } label: {
                    LocationCellView(city: .constant(city), isCelsius: $isCelsius, selected: $selected, showDetails: $showDetails)
                }
            }
        }
        .listStyle(PlainListStyle())
    }
}

struct LocationCellView: View {

    @Binding var city: City
    @Binding var isCelsius: Bool
    @Binding var selected: City?
    @Binding var showDetails: City?

    var body: some View {
        HStack {
            VStack(spacing: 8) {
                HStack {
                    if let temp = city.temperature {
                        Text("\(city.name), \(temp.toString(isCelsius: isCelsius))")
                    }
                    else {
                        Text(city.name)
                    }
                    Spacer()
                }
                HStack {
                    Text(Date.toString(city.updated))
                    Spacer()
                }
            }
            Spacer()

            if city.numberOfData > 1 {
                Button {
                    showDetails = city
                } label: {
                    Image(systemName: "doc.text.magnifyingglass")
                }
                .accessibilityLabel("Show temperature history chart")
            }
        }
        .padding([.top, .bottom], 3)
        .font(.body)
    }
}

struct LocationListView_Previews: PreviewProvider {
    static var previews: some View {
        LocationListView(data: WeatherDataSource(api: StubApi()), isCelsius: .constant(true), selected: .constant(nil), showDetails: .constant(nil))
    }
}
