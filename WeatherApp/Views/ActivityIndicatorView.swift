//
//  ActivityIndicatorView.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/2/22.
//

import UIKit
import SwiftUI

/// Custom activity indicator view
class CustomActivityIndicator: UIView {

    private var activityIndicator: UIActivityIndicatorView!
    private var stopped = false
    private var didShow = false
    private var parentView: UIView?

    /// Initializer
    ///
    /// - Parameters:
    ///   - parentView: the parent view
    ///   - isDark: true - will make the content dark, false - no changes in content
    public init(parentView: UIView?, isDark: Bool = true) {
        super.init(frame: parentView?.bounds ?? UIScreen.main.bounds)

        self.parentView = parentView

        configureIndicator(isDark: isDark)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Configure indicator and changes colors
    ///
    /// - Parameter isDark: true - will make the content dark, false - no changes in content
    private func configureIndicator(isDark: Bool) {
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(activityIndicator)
        self.addConstraint(NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: activityIndicator, attribute: .centerX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: activityIndicator, attribute: .centerY, multiplier: 1, constant: 0))
        activityIndicator.style = .large
        if isDark {
            activityIndicator.color = .white
            activityIndicator.tintColor = .white
            self.backgroundColor = UIColor(white: 0.0, alpha: 0.7)
        }
        else {
            activityIndicator.color = .gray
            activityIndicator.tintColor = .gray
            self.backgroundColor = UIColor.clear
        }
        self.alpha = 0.0
    }

    /// Removes the indicator
    public func stop() {
        stopped = true
        if !didShow { return }
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0.0
        }, completion: { success in
            self.activityIndicator.stopAnimating()
            self.removeFromSuperview()
        })
    }

    /// Show
    /// Usage: `let indicator = ActivityIndicator().start()`
    ///
    /// - Returns: self
    public func start() -> CustomActivityIndicator {
        didShow = true
        if !stopped {
            if let view = parentView {
                self.translatesAutoresizingMaskIntoConstraints = false
                view.addSubview(self)
                view.addConstraint(NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0))
                view.addConstraint(NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0))
                view.addConstraint(NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0))
                view.addConstraint(NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0))
                return self
            }
            (UIApplication.shared.delegate?.window
             ??
             (UIApplication.shared.connectedScenes.first as? UIWindowScene)?.windows.first)?
                .addSubview(self)
        }
        return self
    }

    /// Change alpha after the view is shown
    override public func didMoveToSuperview() {
        activityIndicator.startAnimating()
        UIView.animate(withDuration: 0.3) {
            self.alpha = 0.75
        }
    }
}

final class ActivityIndicatorView: UIViewRepresentable {

    typealias UIViewType = CustomActivityIndicator
    @Environment(\.colorScheme) var colorScheme

    func makeUIView(context: Context) -> CustomActivityIndicator {
        return CustomActivityIndicator(parentView: nil, isDark: colorScheme == .dark).start()
    }

    func updateUIView(_ uiView: CustomActivityIndicator, context: Context) {
    }
}
