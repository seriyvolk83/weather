//
//  MainView.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/2/22.
//

import SwiftUI

/// The main view (selected city details)
struct MainView: View {

    @Binding var city: City
    @Binding var isCelsius: Bool

    var body: some View {
        VStack {
            HStack {
                Text(city.name).font(.largeTitle)
                Spacer()
            }
            HStack {
                if let temperature = city.temperature {
                    if isCelsius {
                        Text("\(temperature.celsius.toTemperatureString())°")
                    }
                    else {
                        Text("\(temperature.fahrenheit.toTemperatureString())°")
                    }
                }
                else {
                    Text("-")
                }
                Spacer()

                // Switcher
                HStack(spacing: 10) {
                    Text("F")
                    ZStack {
                        Toggle("", isOn: $isCelsius).padding(.trailing, 10)
                    }.frame(width: 50)
                    Text("C")
                }
            }
            .font(.title2)
        }
        .foregroundColor(city.temperature?.textColor ?? .black)
        .padding()
        .background(city.temperature?.bgColor ?? .gray)
    }
}

extension Temperature {

    /// the color for the background view that matches the temperature
    var bgColor: Color {
        if celsius < 10 {
            return Color(0x00ccff)
        }
        else if celsius > 25 {
            return .red
        }
        else {
            return .orange
        }
    }

    var textColor: Color {
        if celsius < 10 {
            return Color.black
        }
        else if celsius > 25 {
            return Color.white
        }
        else {
            return Color.black
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView(city: .constant(City.create(name: "Petrozavodsk", temperatureValue: 6)), isCelsius: .constant(false))
    }
}
