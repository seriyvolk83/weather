//
//  SearchResultsView.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/2/22.
//

import SwiftUI

/// the list of search results
struct SearchResultsView: View {

    @StateObject var dataSource = SearchDataSource()
    @Binding var showSearchBar: Bool

    /// the callback used to process the selected search result
    var requestTemperature: (SearchResult) -> Void

    var body: some View {
        ZStack {
            if dataSource.results.isEmpty {
                ZStack {
                    Text(dataSource.isNothingFound ? "Nothing found" : "Type something to search")
                        .opacity(0.5)
                }
            }
            else {
                List {
                    ForEach(dataSource.results, id: \.self) { item in
                        Button {
                            requestTemperature(item)
                            showSearchBar = false
                        } label: {
                            SearchResultCellView(city: .constant(item))
                        }
                    }
                }
            }
            if dataSource.loadingData {
                ActivityIndicatorView()
                    .background(Color.white.opacity(0.5))
            }
        }
    }
}

/// the search result row
struct SearchResultCellView: View {

    @Binding var city: City

    var body: some View {
        HStack {
            VStack(spacing: 8) {
                HStack {
                    Text(city.name)
                    Spacer()
                }
                if let location = city.location {
                    HStack {
                        Text("lat: \(location.latitude), lon: \(location.longitude)")
                        Spacer()
                    }
                }
            }
            Spacer()
        }
        .padding([.top, .bottom], 3)
        .font(.body)
    }
}
