//
//  ChartView.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/3/22.
//

import SwiftUI
import Charts

/// Chart view
struct ChartView: UIViewRepresentable {

    @Binding var temperature: [Temperature]
    @Binding var isCelsius: Bool

    func makeUIView(context: UIViewRepresentableContext<ChartView>) -> LineChartView {
        let chart = LineChartView()
        updateChart(chart: chart)
        return chart
    }

    func updateUIView(_ uiView: LineChartView, context: Context) {
        updateChart(chart: uiView)
    }

    private func updateChart(chart: LineChartView) {
        let data = LineChartData()
        let list = temperature.map{ChartDataEntry(x: $0.time.timeIntervalSince1970, y: isCelsius ? $0.celsius : $0.fahrenheit)}
        let set = LineChartDataSet(entries: list, label: "Temperature")
        data.dataSets = [set]
        chart.data = data

        if list.count > 0 {
            let first = temperature.first!
            let last = temperature.last!
            let dt = last.time.timeIntervalSince1970 - first.time.timeIntervalSince1970

            class DateXAxisFormatter: IAxisValueFormatter {

                private let formatter: DateFormatter

                init(formatter: DateFormatter) {
                    self.formatter = formatter
                }

                func stringForValue(_ value: Double, axis: AxisBase?) -> String {
                    formatter.string(from: Date(timeIntervalSince1970: value))
                }
            }
            
            if dt > 3 * 24 * 60 * 60 {
                chart.xAxis.valueFormatter = DateXAxisFormatter(formatter: Date.ddmm)
            }
            else if dt > 24 * 60 * 60 {
                chart.xAxis.valueFormatter = DateXAxisFormatter(formatter: Date.ddmmhhmm)
            }
            else {
                chart.xAxis.valueFormatter = DateXAxisFormatter(formatter: Date.hhmm)
            }

        }
    }

    func makeCoordinator() -> ChartView.Coordinator {
        let c = Coordinator()
        return c
    }

    class Coordinator: NSObject {
    }
}
