//
//  LocationDetailsView.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/3/22.
//

import SwiftUI

/// Location details view with a chart
struct LocationDetailsView: View {

    @ObservedObject var dataSource: DetailsDataSource
    @Binding var isCelsius: Bool

    var body: some View {
        VStack {
            ChartView(temperature: $dataSource.data, isCelsius: $isCelsius)
                .navigationTitle(dataSource.city?.name ?? "Details")
            if let t = dataSource.minTemperature {
                HStack {
                    Text("Minimum temperature: \(t.toString(isCelsius: isCelsius, digits: 2))")
                    Spacer()
                }
            }
            if let t = dataSource.maxTemperature {
                HStack {
                    Text("Maximum temperature: \(t.toString(isCelsius: isCelsius, digits: 2))")
                    Spacer()
                }
            }
        }
        .padding()
    }
}
