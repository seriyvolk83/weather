//
//  HomeView.swift
//  WeatherApp
//
//  Created by Volkov Alexander on 4/2/22.
//

import SwiftUI
import SwiftEx83

/// Home view
struct HomeView: View {

    /// the selected city (main view is shown for this location)
    @State var selectedCity: City?
    /// toggle model
    @State var isCelsius: Bool = true
    /// the data source
    @StateObject var data = WeatherDataSource(api: RestAPI())

    @State var showDetails: City?

    /// true - show search bar, false - else
    @State private var showSearchBar: Bool = false
    /// search data source
    @StateObject var dataSource = SearchDataSource()

    var body: some View {
        if #available(iOS 15.0, *) {
            getMainView()
                // Loading indicator for iOS>=15
                .overlay {
                    if data.loadingNewData {
                        ActivityIndicatorView()
                            .background(Color.white.opacity(0.5))
                    }
                }
        } else {
            getMainView()
        }
    }

    /// The main view
    private func getMainView() -> some View {
        NavigationView {
            ZStack {
                /// Main view
                if !showSearchBar {
                    VStack(spacing: 0) {
                        if let city = selectedCity {
                            MainView(city: .constant(city),
                                     isCelsius: $isCelsius)
                        }
                        LocationListView(data: data, isCelsius: $isCelsius, selected: $selectedCity, showDetails: $showDetails)
                        Spacer()
                        NavigationLink("", destination: LocationDetailsView(dataSource: DetailsDataSource(city: showDetails), isCelsius: $isCelsius), isActive: Binding<Bool>(get: {
                            showDetails != nil
                        }, set: { value in
                            if !value {
                                showDetails = nil
                            }
                        })).opacity(0)
                    }
                    .navigationBarTitle("Weather", displayMode: .inline)
                    .navigationBarItems(
                        leading: Button(action: {
                            DispatchQueue.main.async {
                                showSearchBar = true
                            }
                        }, label: {
                            Image(systemName: "magnifyingglass")
                        }).accessibilityLabel("Search location"),
                        trailing: Button(action: {
                            useCurrentLocationAction()
                        }, label: {
                            Image(systemName: "location.circle")
                        }).accessibilityLabel("Use current location"))


                }
                /// Search results
                else {
                    SearchResultsView(dataSource: dataSource, showSearchBar: $showSearchBar) { locationToFetch in
                        data.requestWeatherData(for: locationToFetch) {
                            selectedCity = $0
                        }
                    }
                    .toolbar {
                        ToolbarItem(placement: .principal) {
                            SearchBar(text: .constant(""), showSearchBar: $showSearchBar, dataSource: dataSource)
                        }
                    }
                }
                // Loading indicator for iOS<15
                if #available(iOS 15.0, *) {
                }
                else {
                    if data.loadingNewData {
                        ActivityIndicatorView()
                            .background(Color.white.opacity(0.5))
                    }
                }
            }
        }
    }

    /// Use current location action
    private func useCurrentLocationAction() {
        LocationUtil.shared.getCurrentLocation { location in
            data.requestWeatherData(for: location) { city in
                selectedCity = city
            }
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView(selectedCity: City.create(name: "Petrozavodsk", temperatureValue: 6))
    }
}

/// the search bar
struct SearchBar: UIViewRepresentable {

    @Binding var text: String
    @Binding var showSearchBar: Bool
    @ObservedObject var dataSource: SearchDataSource

    class Coordinator: NSObject, UISearchBarDelegate {

        @Binding var text: String
        @Binding var showSearchBar: Bool
        @ObservedObject var dataSource: SearchDataSource

        init(text: Binding<String>, showSearchBar: Binding<Bool>, dataSource: SearchDataSource) {
            self._text = text
            self._showSearchBar = showSearchBar
            self.dataSource = dataSource
        }

        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            text = searchText
        }

        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            showSearchBar = false
        }

        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            dataSource.search(for: searchBar.text ?? "")
        }
    }

    func makeCoordinator() -> SearchBar.Coordinator {
        return Coordinator(text: $text, showSearchBar: $showSearchBar, dataSource: dataSource)
    }

    func makeUIView(context: UIViewRepresentableContext<SearchBar>) -> UISearchBar {
        let searchBar = UISearchBar(frame: .zero)
        searchBar.delegate = context.coordinator
        searchBar.showsCancelButton = true
        searchBar.autocapitalizationType = .none
        searchBar.becomeFirstResponder()
        return searchBar
    }

    func updateUIView(_ uiView: UISearchBar, context: UIViewRepresentableContext<SearchBar>) {
        uiView.text = text
    }
}
