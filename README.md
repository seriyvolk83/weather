# Weather sample app

## Deployment dependencies

Before performing a Deployment, it is assumed that the following have been set up:

- Xcode 13.0+
- iPhone with iOS 14+

## Organization of Submission
- `WeatherApp.xcworkspace` – Xcode workspace to open.
- `README.md` – this file

## 3rd party Libraries

- [`Charts`](https://github.com/danielgindi/Charts) is used to show a chart
- [`SwiftEx83`](https://gitlab.com/seriyvolk83/SwiftEx) is used to simplify API implementation

## Configuration

Configuration is provided in `configuration.plist` file stored in `WeatherApp/Supporting Files` group.
- `apiKey` - API key
- `apiBase` - API base URL

## Deployment

Configure the app properly (see "Configuration" above).

Run the following command from the root directory to install the dependencies:
```
pod install
```

### Run the app

To build and run the app on a real device or iPad simulator you will need to do the following:

1. Open `WeatherApp.xcworkspace` in Xcode
2. Select *WeatherApp* scheme from the top left drop down list to run on iPhone or simulator.
3. Select your connected iPhone or simulator from the top left dropdown list to run corresponding version of the app.
4. Click menu Product -> Run (Cmd+R)

![Main screen](docs/img1.jpg)

![Details screen](docs/img2.jpg)
